# Design Brief

Living brief for visual design topics, split into phases. To be evolved as phases complete.

The [brief itself](./design_brief.md) is also in this repository. [Assets](/interpeer/design-assets) live in their separate repository.