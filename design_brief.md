This document serves as a creative brief for visual designers
from the Interpeer Project (currently represented by Jens Finkhaeuser, to
be transferred to an appropriate NGO later).

The creative work to be undertaken covers several target assets to be created
in distinct project phases, each with their own commercial proposal. As the
targets relate to each other, however, common elements of the brief are
provided up-front to guide the direction of the work from the beginning.

Each proposal covers the next/a named project phase. Later phases may require
adjustment to this brief from lessons learned. This should therefore be
considered a "living" document, where the latest version is always leading. The
order of the phases can similarly be re-arranged.

Customer Information
====================

Name:             Interpeer Project
Represented by:   Jens Finkhaeuser (to be changed later)

The Interpeer Project is an umbrella for several open source software projects
that together aim to provide a stack for a next generation, human centric
internet. It is funded by public money and/or donations, and thus must make all
products and research results public. This communal aspect is central to the
project's vision, independent of the funding sources.

The project's premise is that the current web architecture encourages
centralization of data processing, and therefore the involvement of third
parties in any human-to-human interactions whether conceptually necessary or
not. This leads to the temptation to exploit these gatekeeper positions. In
order to protect privacy and safety on the future Internet, such gatekeeper
positions should be discouraged and made unnecessary as much as technically
possible.

This should be as widely accessible as possible, which requires standardization
of protocols, as well as targeting a wide range of operating system and hardware
platforms, such that most computer users will be able to find a way to use the
system.

Competitors
===========

A full competitor analysis is difficult. There are more distributed/
decentralized projects around that one can count. However, most of them appear
to center around a particular product or platform with its own philosophy
attached. For example, the [GNUnet project](https://www.gnunet.org/en/) by its
own admission is not particularly interested in supporting non-free OS
platforms, and intends to act as a basis for a cryptocurrency. Neither are
shared by the interpeer project. Any of the web3 decentralized projects will
largely have the same attributes.

SOLID is different in that it pursues many of the same high level goals, but
does so building on top of existing, centralized protocols. It seems unlikely
that the level of control over personal data is achievable in this fashion that
a truly privacy friendly future Internet should offer.

A fair if brief summary of competitors would be that to this date, it's been
impossible to find a project that aims at a web-/internet protocol stack
replacement with the same amount of standardization work, and relativey little
interest in shaping how the stack shall be used.

Target Audience
===============

The ultimate target audience for Interpeer Project software is every computer
user. But that is near everyone on the planet.

The initial target audiences more easily defined:

- Researchers, both academic and commercial, should receive information and
  be engaged in exchanges on the research topics.
- Developers who may wish to contribute to the project.
- Developers who may wish to use the projcect's software in their respective
  projects/products.

The combining characteristic between these groups is that it's fair to expect
them to process complex information not limited to tweet length snippets.
Still, as the expression goes, a picture says more than a thousand words, and
these groups are no exception. The point remains, however, that *at this stage
of the project*, it is less necessary to reduce designs to simple headlines and
graphics, and text-heavy publications in form of papers or documentation/
websites is currently the kind of content most likely to be produced.

Phase 1: Umbrella Logo
======================

The overall Interpeer Project is in need of a Logo.

The logo should embody the overall motivation for the project, to enable safe
and privacy friendly human-to-human interactions on the Internet without third
parties (where feasible).

Keywords:

- Interconnectedness
- Community
- Non-hierarchical (peers in the non-computer sense)

Requirements
------------

- Scalable (vector) graphic
- Must work by shape alone, colours supplement the design rather
- Must work somewhat at highly reduced pixellated size (think website favicon).
  Favicons can exist in many resolutions, and modern systems pick reasonably
  large ones. However, in e.g. browser tabs, the 16x16 pixel image sizes are
  still (typically) displayed. In addition to the shape requirement above, a
  reduction to this size should be reasonably recognizable.
- Variations as in [Logo Variations](https://logo.com/blog/logo-variations)
  (only of the final result)
- Consider phase 3/project logos

Process
-------

1. Designer to produce 10-20 *very rough* sketches. Literal pen scribbles on
   paper photographed & emailled are sufficient. This is to generate ideas to
   discuss.
2. Feedback talk; produces instructions for more detailed sketches in next
   step, based on the initial products or ideas generated from this discussion.
3. Designer to produce 3-5 sketches for discussing the final elements, shape
   and colour scheme of the logo. This may include variations on colour scheme
   or composition.
4. Feedback talk; this should produce the instructions for the final design.
5. Designer to produce final design.

At most one more round of discussions (steps 3-4) should suffice before the final
step. This is to be undertaken only by mutual agreement of both parties.

Competitors
-----------

- The GNUnet (gnunet.org) logo uses a graph structure to show interconnectedness,
  and shapes the graph roughly as the GNU logo (the head of a gnu). This is a
  clever logo for this project. The graph is a good metaphor for
  interconnectedness, but it also quite widely used, which is a pro and a con
  for this logo.
- The Ubuntu (ubuntu.com) logo is a good, abstract representation of community.
  At first glance, it is more recognizable as a circle. But the circle is made
  up of three segments, each representing the arms of a person viewed from the
  top, such that the circle is made up of three people holding hands.

Phase 2: Brand Book
===================

The project is expected to publish a main website, as well as sub-project
specific websites, papers and presentations. These should all have some
coherence/relate to each other. It's therefore best to provide a brand book/
style guide early on in the co-operation, to cover some design decisions up
front.

As later phases intend to dive deeper into these individual items, it is
unreasonable to expect this brand book to be complete from the start - it is
also not meant to be a detailled style guide as one can find for e.g. Material
Design or similar.

It should, however, cover basic colour schemes, font choices and general brand
identity topics. The purpose of the Brand Book is to enable work picked up in
a later phase to be consistent with earlier decisions, so serves as the
institutional memory of design decisions made.

Requirements and process to be determined later.

Phase 3: Sub-Project Logo Variations (multiple)
===============================================

The overall Interpeer Project is sub-divided into several code projects that may
each be used outside of the Interpeer umbrella. A distinct logo for each such
project is desirable.

It would be very interesting if project logos incorporated an element of the
umbrella logo (shape/colour/both), or was a variation of the umbrella logo.
The process by which such derivative logos should be designed should then become
part of the Brand Book.

As this may mean the best choice is to design the phase 1 logo appropriately,
this must be taken into consideration early on.

Requirements
------------

- Must follow the same guide lines as the umbrella logo from phase 1.
- Must fit the Brand Book -and/or- include extending the Brand Book as necessary.

Process
-------

To be determined later, but expected to be similar to phase 1 logo.

Examples
--------

These examples list reasonably well known logos that follow a common theme.

- [Libre Office](https://www.elgrupoinformatico.com/noticias/descarga-libreoffice-con-mejor-compatibilidad-con-office-t76550.html)
- [Microsoft Office](http://www.newdesignfile.com/post_microsoft-office-2013-logos-vector_318706/)

Phase 4: Website Design
=======================

The Intepeer Project hosts a website which is to be extended to cover more
content. The design should cover at least the following sections and elements.
The work must also provide for HTML templates for a static website generator
(to be determined, most likely Pelican and/or Sphinx).

Sections:

- Main landing page
- Blog
  - Latest
  - Individual posts
  - Archive
- Sub-Projects:
  - Landing page
  - Documentation
    - Download/installation
    - Usage/API (this implies multiple sections and navigation as in e.g.
      readthedocs.org)

- Elements:
  - Bespoke elements for landing page
  - Call-out boxes (i.e. do something related by clicking here)
  - Notes/warning/tip boxes (may include some logos/fontawesome)
  - Code embeds (blocks/inline)
  - Bullet/numbered/itemized lists, nested
  - Graphic embeds
  - Video embeds


Requirements
------------

- Brand Book updated with style decisions
- Static site generator templates (e.g. Pelican)
- Documentation engine templates (e.g. Sphinx)
- For main content, expect input from e.g. Markdown to be processed. The landing
  page can be completely bespokse, the project landing pages more simply
  structured.

Process
-------

TBD


Phase 5: Complete Brand Identity (various)
==========================================

Based on the decisions made in the Brand Book/previous phases, create a complete
brand identity. The below requirements are an initial list of elements, to be
discussed at each phase of the co-operation. Each element should lead to the
Brand Book to be updated.

It is expected that each or most of the below will be covered in individual
phases.

Requirements
------------

- Letterhead
- Business cards
- Slide templates
  - 16:9 / 4:3 compatible
  - Specific requirements to be determined
- Video overlay templates
  - Mainly to be used for recordings of talks, so e.g. should
    have an overlay for talk title/location/date, person speaking,
    topic under discussion, etc.
